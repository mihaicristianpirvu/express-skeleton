console.log("Client-side code running");
const inButton = document.getElementById("inButton");
const outDiv = document.getElementById("outDiv");
var clickCount = 0;

(function() {
  /* Main function of this code. */

  /* When pressing 'click', send the data to the server. */
  inButton.addEventListener("click", (e) => {
    console.log("button was clicked");
    clickCount += 1;
    outDiv.innerHTML = `Clicked ${clickCount} times`
  })

})()
